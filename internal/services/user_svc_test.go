package services_test

import (
	"reflect"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
	"technical-test-gokomodo/internal/services"
	"testing"
)

func TestNewLoginService(t *testing.T) {
	var tests []struct {
		name string
		want repo.UserRepositoryService
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := services.NewLoginService(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewLoginService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceLogin_SignUp(t *testing.T) {
	type args struct {
		user dto.UserBuyer
	}
	var tests []struct {
		name    string
		s       *services.ServiceLogin
		args    args
		want    string
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.SignUp(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceLogin.SignUp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("serviceLogin.SignUp() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceLogin_Login(t *testing.T) {
	type args struct {
		email    string
		password string
		role     string
	}
	var tests []struct {
		name    string
		s       *services.ServiceLogin
		args    args
		want    string
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.Login(tt.args.email, tt.args.password, tt.args.role)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceLogin.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("serviceLogin.Login() = %v, want %v", got, tt.want)
			}
		})
	}
}

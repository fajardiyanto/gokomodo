package services

import (
	"github.com/google/uuid"
	"technical-test-gokomodo/config"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
)

type ServiceProduct struct{}

func NewProductService() repo.ProductRepositoryService {
	return &ServiceProduct{}
}

func (*ServiceProduct) GetProducts(id string) (*[]dto.Product, error) {
	var data *[]dto.Product
	if err := config.GetDBConn().Orm().Debug().Model(dto.Product{}).Where("seller = ?", id).Find(&data).Error; err != nil {
		return nil, err
	}

	return data, nil
}

func (*ServiceProduct) AddProduct(product dto.Product) (*dto.Product, error) {
	product.ID = uuid.NewString()

	if err := config.GetDBConn().Orm().Debug().Model(&dto.Product{}).Create(&product).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &product, nil
}

func (*ServiceProduct) GetOrderList(id string) (*[]dto.Order, error) {
	var data *[]dto.Order
	if err := config.GetDBConn().Orm().Debug().Model(dto.Order{}).Where("seller = ?", id).Find(&data).Error; err != nil {
		return nil, err
	}

	return data, nil
}

func (*ServiceProduct) AcceptOrder(id, sellerID string) error {
	if err := config.GetDBConn().Orm().Debug().Model(&dto.Order{}).Where("id = ?", id).Updates(map[string]interface{}{
		"status": "accepted",
		"seller": sellerID,
	}).Error; err != nil {
		config.GetLogger().Error(err)
		return err
	}

	return nil
}

func (*ServiceProduct) OrderProduct(order dto.Order) (*dto.Order, error) {
	order.ID = uuid.NewString()

	if err := config.GetDBConn().Orm().Debug().Model(&dto.Order{}).Create(&order).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &order, nil
}

func (*ServiceProduct) ViewBuyerOrderList(id string) (*[]dto.Order, error) {
	var data *[]dto.Order
	if err := config.GetDBConn().Orm().Debug().Model(dto.Order{}).Where("buyer = ?", id).Find(&data).Error; err != nil {
		return nil, err
	}

	return data, nil
}

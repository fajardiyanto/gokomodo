package services

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	"technical-test-gokomodo/config"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
	"technical-test-gokomodo/pkg/auth"
	"technical-test-gokomodo/pkg/util"
)

type ServiceLogin struct{}

func NewLoginService() repo.UserRepositoryService {
	return &ServiceLogin{}
}

func (*ServiceLogin) SignUp(user dto.UserBuyer) (string, error) {
	pass, err := util.Hash(user.Password)
	if err != nil {
		return "", err
	}

	user.ID = uuid.NewString()
	user.Password = string(pass)

	if err = config.GetDBConn().Orm().Table("buyer").Debug().Model(&dto.UserBuyer{}).Create(&user).Error; err != nil {
		config.GetLogger().Error(err)
		return "", err
	}

	token, err := auth.CreateToken(user.ID, "buyer")
	if err != nil {
		return "", err
	}

	return token, nil
}

func (*ServiceLogin) Login(email, password, role string) (string, error) {
	var table string = "seller"
	if role == "buyer" {
		table = "buyer"
	}

	var user dto.UserBuyer
	if err := config.GetDBConn().Orm().Debug().Table(table).Model(dto.UserBuyer{}).Where("email = ?", email).Take(&user).Error; err != nil {
		return "", errors.New("user not found")
	}

	if err := util.VerifyPassword(user.Password, password); err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", errors.New("password not match")
	}

	token, err := auth.CreateToken(user.ID, table)
	if err != nil {
		return "", err
	}

	return token, nil
}

package services

import (
	"fmt"

	"technical-test-gokomodo/config"

	"github.com/shirou/gopsutil/v3/host"
	"technical-test-gokomodo/internal/repo"
)

type Service struct{}

func NewHealthInfoService() repo.HealthInfoRepositoryService {
	return &Service{}
}

func (*Service) BuildInfo() (OSBuildName string) {
	if info, err := host.Info(); err == nil {
		OSBuildName = fmt.Sprintf("%s (%s)",
			info.PlatformFamily, info.PlatformVersion)
	}

	return OSBuildName
}

func (*Service) HealthInfoDatabase() bool {
	var version string
	err := config.GetDBConn().Orm().Raw("SELECT @@VERSION").Scan(&version).Error
	if err != nil {
		return false
	}

	return true
}

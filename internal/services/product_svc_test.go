package services_test

import (
	"reflect"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
	"technical-test-gokomodo/internal/services"
	"testing"
)

func TestNewProductService(t *testing.T) {
	var tests []struct {
		name string
		want repo.ProductRepositoryService
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := services.NewProductService(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProductService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceProduct_GetProducts(t *testing.T) {
	type args struct {
		id string
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		want    *[]dto.Product
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.GetProducts(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.GetProducts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("serviceProduct.GetProducts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceProduct_AddProduct(t *testing.T) {
	type args struct {
		product dto.Product
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		want    *dto.Product
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.AddProduct(tt.args.product)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.AddProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("serviceProduct.AddProduct() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceProduct_GetOrderList(t *testing.T) {
	type args struct {
		id string
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		want    *[]dto.Order
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.GetOrderList(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.GetOrderList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("serviceProduct.GetOrderList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceProduct_AcceptOrder(t *testing.T) {
	type args struct {
		id       string
		sellerID string
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.s.AcceptOrder(tt.args.id, tt.args.sellerID); (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.AcceptOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_serviceProduct_OrderProduct(t *testing.T) {
	type args struct {
		order dto.Order
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		want    *dto.Order
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.OrderProduct(tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.OrderProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("serviceProduct.OrderProduct() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_serviceProduct_ViewBuyerOrderList(t *testing.T) {
	type args struct {
		id string
	}
	var tests []struct {
		name    string
		s       *services.ServiceProduct
		args    args
		want    *[]dto.Order
		wantErr bool
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.ViewBuyerOrderList(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("serviceProduct.ViewBuyerOrderList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("serviceProduct.ViewBuyerOrderList() = %v, want %v", got, tt.want)
			}
		})
	}
}

package services_test

import (
	"reflect"
	"technical-test-gokomodo/internal/repo"
	healthinfo_svc "technical-test-gokomodo/internal/services"
	"testing"
)

func TestNewHealthInfoService(t *testing.T) {
	var tests []struct {
		name string
		want repo.HealthInfoRepositoryService
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := healthinfo_svc.NewHealthInfoService(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewHealthInfoService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_service_BuildInfo(t *testing.T) {
	var tests []struct {
		name            string
		s               *healthinfo_svc.Service
		wantOSBuildName string
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotOSBuildName := tt.s.BuildInfo(); gotOSBuildName != tt.wantOSBuildName {
				t.Errorf("service.BuildInfo() = %v, want %v", gotOSBuildName, tt.wantOSBuildName)
			}
		})
	}
}

func Test_service_HealthInfoDatabase(t *testing.T) {
	var tests []struct {
		name string
		s    *healthinfo_svc.Service
		want bool
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.HealthInfoDatabase(); got != tt.want {
				t.Errorf("service.HealthInfoDatabase() = %v, want %v", got, tt.want)
			}
		})
	}
}

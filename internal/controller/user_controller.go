package controller

import (
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"net/http"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
)

type UserController struct {
	svc repo.UserRepositoryService
}

func NewUserController(svc repo.UserRepositoryService) UserController {
	return UserController{
		svc: svc,
	}
}

func (s *UserController) SignUp(w http.ResponseWriter, r *http.Request) error {
	req := dto.UserBuyer{}

	err := interfaces.GetJSON(r, &req)
	if err != nil {
		return interfaces.JSON(w, http.StatusBadRequest, interfaces.APIResponseError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	token, err := s.svc.SignUp(req)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, map[string]interface{}{
		"token": token,
	})
}

func (s *UserController) Login(w http.ResponseWriter, r *http.Request) error {
	role := interfaces.GetParam(r, "role")
	req := dto.UserBuyer{}

	err := interfaces.GetJSON(r, &req)
	if err != nil {
		return interfaces.JSON(w, http.StatusBadRequest, interfaces.APIResponseError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	token, err := s.svc.Login(req.Email, req.Password, role)
	if err != nil {
		return interfaces.JSON(w, http.StatusUnauthorized, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, map[string]interface{}{
		"token": token,
	})
}

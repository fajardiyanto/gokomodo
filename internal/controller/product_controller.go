package controller

import (
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"net/http"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
	"technical-test-gokomodo/pkg/auth"
)

type ProductController struct {
	svc repo.ProductRepositoryService
}

func NewProductController(svc repo.ProductRepositoryService) ProductController {
	return ProductController{
		svc: svc,
	}
}

func (s *ProductController) GetProducts(w http.ResponseWriter, r *http.Request) error {
	uid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	res, err := s.svc.GetProducts(uid)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
		Data:    res,
	})
}

func (s *ProductController) AddProduct(w http.ResponseWriter, r *http.Request) error {
	req := dto.Product{}

	err := interfaces.GetJSON(r, &req)
	if err != nil {
		return interfaces.JSON(w, http.StatusBadRequest, interfaces.APIResponseError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	uid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	req.Seller = uid

	res, err := s.svc.AddProduct(req)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
		Data:    res,
	})
}

func (s *ProductController) GetOrderList(w http.ResponseWriter, r *http.Request) error {
	uid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	res, err := s.svc.GetOrderList(uid)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
		Data:    res,
	})
}

func (s *ProductController) AcceptOrder(w http.ResponseWriter, r *http.Request) error {
	uid := interfaces.GetQuery(r, "id")

	sid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	if err = s.svc.AcceptOrder(uid, sid); err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
	})
}

func (s *ProductController) OrderProduct(w http.ResponseWriter, r *http.Request) error {
	req := dto.Order{}

	err := interfaces.GetJSON(r, &req)
	if err != nil {
		return interfaces.JSON(w, http.StatusBadRequest, interfaces.APIResponseError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	uid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	req.Buyer = uid

	res, err := s.svc.OrderProduct(req)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
		Data:    res,
	})
}

func (s *ProductController) ViewBuyerOrderList(w http.ResponseWriter, r *http.Request) error {
	uid, err := auth.ExtractTokenId(r)
	if err != nil {
		return interfaces.JSON(w, http.StatusNonAuthoritativeInfo, interfaces.APIResponseError{
			Code:    http.StatusNonAuthoritativeInfo,
			Message: err.Error(),
		})
	}

	res, err := s.svc.ViewBuyerOrderList(uid)
	if err != nil {
		return interfaces.JSON(w, http.StatusInternalServerError, interfaces.APIResponseError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return interfaces.JSON(w, http.StatusOK, dto.ResponseSuccess{
		Code:    http.StatusOK,
		Message: "success",
		Data:    res,
	})
}

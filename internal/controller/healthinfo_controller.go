package controller

import (
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"net/http"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/repo"
)

type HealthInfoController struct {
	svc repo.HealthInfoRepositoryService
}

func NewHealthInfoController(svc repo.HealthInfoRepositoryService) HealthInfoController {
	return HealthInfoController{
		svc: svc,
	}
}

func (c *HealthInfoController) HealthInfo(w http.ResponseWriter, r *http.Request) error {
	res := &dto.ResponseHealthInfo{
		ServiceName:    dto.GetConfig().Name,
		ServiceVersion: dto.GetConfig().Version,
		Compiler:       c.svc.BuildInfo(),
		Database: dto.Database{
			Mysql: c.svc.HealthInfoDatabase(),
		},
	}
	return interfaces.JSON(w, http.StatusOK, res)
}

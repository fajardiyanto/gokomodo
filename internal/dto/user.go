package dto

type UserBuyer struct {
	ID             string `json:"id" gorm:"column:id"`
	Email          string `json:"email" gorm:"column:email"`
	Name           string `json:"name" gorm:"column:name"`
	Password       string `json:"password" gorm:"column:password"`
	AlamatPengirim string `json:"alamat_pengirim" gorm:"column:alamat_pengirim"`
}

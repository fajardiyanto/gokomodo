package dto

type Order struct {
	ID                         string  `json:"id" gorm:"column:id"`
	Buyer                      string  `json:"buyer" gorm:"column:buyer"`
	Seller                     string  `json:"seller" gorm:"column:seller"`
	DeliverySourceAddress      string  `json:"delivery_source_address" gorm:"column:delivery_source_address"`
	DeliveryDestinationAddress string  `json:"delivery_destination_address" gorm:"column:delivery_destination_address"`
	Item                       int32   `json:"items" gorm:"column:items"`
	Quantity                   int32   `json:"quantity" gorm:"column:quantity"`
	Price                      float64 `json:"price" gorm:"column:price"`
	TotalPrice                 string  `json:"total_price" gorm:"column:total_price"`
	Status                     string  `json:"status" gorm:"column:status"`
}

func (*Order) TableName() string {
	return "order_product"
}

package dto

type Product struct {
	ID          string  `json:"id" gorm:"column:id"`
	ProductName string  `json:"product_name" gorm:"column:product_name"`
	Description string  `json:"description" gorm:"column:description"`
	Price       float64 `json:"price" gorm:"column:price"`
	Seller      string  `json:"seller" gorm:"column:seller"`
}

func (*Product) TableName() string {
	return "product"
}

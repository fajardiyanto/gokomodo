package repo

import "technical-test-gokomodo/internal/dto"

type UserRepositoryService interface {
	SignUp(user dto.UserBuyer) (string, error)
	Login(email, password, role string) (string, error)
}

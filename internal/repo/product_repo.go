package repo

import "technical-test-gokomodo/internal/dto"

type ProductRepositoryService interface {
	GetProducts(id string) (*[]dto.Product, error)
	AddProduct(product dto.Product) (*dto.Product, error)
	GetOrderList(id string) (*[]dto.Order, error)
	AcceptOrder(id, sellerID string) error
	OrderProduct(order dto.Order) (*dto.Order, error)
	ViewBuyerOrderList(id string) (*[]dto.Order, error)
}

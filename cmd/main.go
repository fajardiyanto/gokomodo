package main

import (
	"github.com/fajarardiyanto/flt-go-router/lib"
	"technical-test-gokomodo/config"
	"technical-test-gokomodo/internal/controller"
	"technical-test-gokomodo/internal/dto"
	"technical-test-gokomodo/internal/services"
	"technical-test-gokomodo/pkg/middleware"
)

func main() {
	// init configuration database
	config.Init()

	r := lib.New(dto.GetConfig().Version)

	r.Use(middleware.MiddlewareError())

	{
		// Service
		svc := services.NewLoginService()
		svcHealth := services.NewHealthInfoService()

		// Controller
		ctrl := controller.NewUserController(svc)
		health := controller.NewHealthInfoController(svcHealth)

		r.GET("/health-info", health.HealthInfo)

		r.POST("/sign-up", ctrl.SignUp)
		r.POST("/:role/login", ctrl.Login)

	}

	r.Use(middleware.SetMiddlewareAuthentication())

	{
		// Service
		svc := services.NewProductService()

		// Controller
		ctrl := controller.NewProductController(svc)

		r.GET("/products", ctrl.GetProducts)
		r.POST("/add/products", ctrl.AddProduct)
		r.GET("/order/list", ctrl.GetOrderList)
		r.PUT("/accept/order", ctrl.AcceptOrder)

		r.POST("/order/product", ctrl.OrderProduct)
		r.GET("/buyer/order/list", ctrl.ViewBuyerOrderList)
	}

	r.Run(dto.GetConfig().Port)
}

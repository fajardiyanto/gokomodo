package middleware

import (
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"github.com/pkg/errors"
	"net/http"
	"technical-test-gokomodo/config"
	"technical-test-gokomodo/pkg/auth"
)

func SetMiddlewareAuthentication() interfaces.MiddlewareFunc {
	return func(next interfaces.Handler) interfaces.Handler {
		return func(w http.ResponseWriter, r *http.Request) error {
			err := auth.TokenValid(r)
			if err != nil {
				return interfaces.JSON(w, http.StatusUnauthorized, map[string]interface{}{
					"msg": "Unauthorized",
				})
			}
			next(w, r)

			return nil
		}
	}

}

func MiddlewareError() interfaces.MiddlewareFunc {
	return func(next interfaces.Handler) interfaces.Handler {
		return func(w http.ResponseWriter, r *http.Request) error {
			if err := next(w, r); err != nil {
				response := formatResponse(err)

				if err = interfaces.JSON(w, response.Code, response); err != nil {
					return err
				}

				config.GetLogger().Error(err).Quit()
			}
			return nil
		}
	}
}

// formatResponse returns response error format based on error type.
func formatResponse(err error) *interfaces.APIResponseError {
	switch et := errors.Cause(err).(type) {
	case *interfaces.RequestError:
		return interfaces.NewAPIResponseError(et.Error(), et.Status)
	case interfaces.ValidationError:
		return interfaces.NewAPIValidationError(et, http.StatusBadRequest)
	default:
		return interfaces.NewAPIResponseError(
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
	}
}

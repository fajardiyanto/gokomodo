package config

import (
	databaseinterface "github.com/fajarardiyanto/flt-go-database/interfaces"
	databaseLib "github.com/fajarardiyanto/flt-go-database/lib"
	"technical-test-gokomodo/internal/dto"
)

var database databaseinterface.SQL

func InitMysql(db databaseinterface.Database) {
	database = db.LoadSQLDatabase(dto.GetConfig().Database.Mysql)

	if err := database.LoadSQL(); err != nil {
		logger.Error(err).Quit()
	}
}

func Init() {
	db := databaseLib.NewLib()
	db.Init(GetLogger())

	InitMysql(db)
}

func GetDBConn() databaseinterface.SQL {
	return database
}

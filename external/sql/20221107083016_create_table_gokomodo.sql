-- +goose Up
-- +goose StatementBegin
CREATE TABLE buyer (
   id varchar(50) primary key ,
   email varchar(255) ,
   name varchar(255) ,
   password varchar(255) ,
   alamat_pengirim text
);

CREATE TABLE seller (
    id varchar(50) primary key ,
    email varchar(255) ,
    name varchar(255) ,
    password varchar(255) ,
    alamat_pickup text
);

CREATE TABLE product (
     id varchar(50) primary key ,
     product_name varchar(255) ,
     description text ,
     price float8 ,
     seller varchar(50)
);

CREATE TABLE order_product (
       id varchar(50) primary key ,
       buyer varchar(50),
       seller varchar(50),
       delivery_source_address text,
       delivery_destination_address text,
       items int,
       quantity int,
       price float8,
       total_price float8,
       status enum('pending', 'accepted')
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE buyer;
DROP TABLE seller;
DROP TABLE product;
DROP TABLE order_product;
-- +goose StatementEnd

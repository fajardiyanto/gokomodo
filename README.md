# Technical Test Gokomodo

## Getting started
Required Golang 1.17+
### Run
```bash
make run
```

### Build Docker
```bash
make docker-build
```

### Run Docker
```bash
make build-run
```

#### Tips
Maybe it would be better to do some basic code scanning before pushing to the repository.
```sh
# for *.nix users just run gosec.sh
# curl is required
# more information https://github.com/securego/gosec
make scan
```